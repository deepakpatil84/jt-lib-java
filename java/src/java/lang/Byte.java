/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * Original source from Google Web Toolkit(GWT) 
 * http://code.google.com/webtoolkit/
 *
 * Modified to adapt JsType Framework
 */

/* Original GWT License */

/*
 * Copyright 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package java.lang;

import com.jstype.fx.EnsureIdentifier;

import java.io.Serializable;

public final class Byte extends Number implements  Serializable,Comparable<Byte> {

    public static final byte MIN_VALUE = (byte) 0x80;
    public static final byte MAX_VALUE = (byte) 0x7F;
    public static final int SIZE = 8;
    @EnsureIdentifier
    private final byte value;

    public Byte(byte value) {
        this.value = value;
    }

    public Byte(String s) {
        value = parseByte(s);
    }

    @Override
    public byte byteValue() {
        return value;
    }

    public int compareTo(Byte b) {
        if (value < b.value) {
            return -1;
        } else if (value > b.value) {
            return 1;
        } else {
            return 0;
        }
    }
    
    //TODO:NEED_CHECK
    public static Byte decode(String s) throws NumberFormatException {       
        return Byte.valueOf((byte) getIntValue(s, MIN_VALUE, MAX_VALUE));
    }

    @Override
    public double doubleValue() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Byte) && (((Byte) obj).value == value);
    }

    @Override
    public float floatValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public int intValue() {
        return value;
    }

    @Override
    public long longValue() {
        return value;
    }

    //TODO:NEED_CHECK
    public static byte parseByte(String s) throws NumberFormatException {
        return parseByte(s, 10);
    }

    //TODO:NEED_CHECK
    public static byte parseByte(String s, int radix) throws NumberFormatException {       
        return (byte) parseIntValue(s, radix, MIN_VALUE, MAX_VALUE);
    }

    @Override
    public short shortValue() {
        return value;
    }

    @Override
    public String toString() {
        return ""+value;
    }

    public static String toString(byte b) {
        return ""+b;
    }

    //TODO:NEED_CHECK
    public static Byte valueOf(byte b) {        
        Byte cachedvalue = ByteCache.cache[b+128];
        if (cachedvalue == null) {
            cachedvalue = ByteCache.cache[b+128] = new Byte(b);
        }
        return cachedvalue;
    }

    public static Byte valueOf(String s) throws NumberFormatException {
        return Byte.valueOf(Byte.parseByte(s));
    }

    public static Byte valueOf(String s, int radix) throws NumberFormatException {
        return Byte.valueOf(Byte.parseByte(s, radix));
    }

    //TODO:NEED_CHECK
    private static class ByteCache {
        private static Byte[] cache = new Byte[256];
    }
}
