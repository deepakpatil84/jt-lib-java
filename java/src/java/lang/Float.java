/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * Original source from Google Web Toolkit(GWT) 
 * http://code.google.com/webtoolkit/
 *
 * Modified to adapt JsType Framework
 */

/* Original GWT License */

/*
 * Copyright 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package java.lang;

import com.jstype.fx.EnsureIdentifier;

/**
 * Wrapper class for float
 * Reference:http://download.oracle.com/javase/6/docs/api/java/lang/Float.html
 */
public final class Float extends Number implements Comparable<Float> {

   
    public static final float MAX_EXPONENT = 127;
    public static final float MAX_VALUE = 3.4028235e+38f;
    public static final float MIN_EXPONENT = -126;
    public static final float MIN_NORMAL = 1.1754943508222875E-38f;
    public static final float NaN = 0f / 0f;
    public static final float NEGATIVE_INFINITY = -1f / 0f;
    public static final float POSITIVE_INFINITY = 1f / 0f;
    public static final int SIZE = 32;

    @EnsureIdentifier
    private final transient float value;
    public Float(double value) {
        this.value = (float) value;
    }

    public Float(float value) {
        this.value = value;
    }
    public Float(String s) {
        value = parseFloat(s);
    }
    @Override
    public byte byteValue() {
        return (byte) value;
    }

    public static int compare(float f1, float f2) {
        if (f1 < f2) {
            return -1;
        } else if (f1 > f2) {
            return 1;
        } else {
            return 0;
        }
    }

    public int compareTo(Float anotherFloat) {
       return compare(value, anotherFloat.value);
    }

    @Override
    public double doubleValue() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Float) && (((Float) obj).value == value);
    }


    @Override
    public float floatValue() {
        return value;
    }

    public static int hashCode(float f) {
        return (int) f;
    }
    @Override
    public int intValue() {
        return (int) value;
    }

    public boolean isInfinite() {
        return isInfinite(value);
    }

    /**
     * Reference:http://www.w3schools.com/jsref/jsref_isfinite.asp
     */
    public static native boolean isInfinite(float v) /*-{
    return !isFinite(v);
    }-*/;
    public boolean isNaN() {
        return isNaN(value);
    }

    /**
     * Reference:http://www.w3schools.com/jsref/jsref_isnan.asp
     */
    public static native boolean isNaN(float x) /*-{
    return isNaN(x);
    }-*/;

    @Override
    public long longValue() {
        return (long) value;
    }

    public static float parseFloat(String s) throws NumberFormatException {
        double doubleValue = parseDoubleValue(s);
        if (doubleValue > Float.MAX_VALUE) {
            return Float.POSITIVE_INFINITY;
        } else if (doubleValue < -Float.MAX_VALUE) {
            return Float.NEGATIVE_INFINITY;
        }
        return (float) doubleValue;
    }

    @Override
    public short shortValue() {
        return (short) value;
    }
    @Override
    public String toString() {
        return toString(value);
    }


    public static String toString(float f) {
        return String.valueOf(f);
    }

    public static Float valueOf(float f) {
        return new Float(f);
    }

    public static Float valueOf(String s) throws NumberFormatException {
        return new Float(parseFloat(s));
    }

    @Override
    public int hashCode() {
        return hashCode(value);
    }
}
