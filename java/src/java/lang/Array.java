/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package java.lang;


import com.jstype.fx.EnsureIdentifier;
import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;

@NativeNonInstanciable(proto="Array")
public class Array {

    @NoJavaScript
    public static native <T> T[] createFrom(T[] array, int length);
    /**
     * length(size) of array
     */
    @EnsureIdentifier
    public int length;
    
    public native static boolean is(Object o)/*-{
    	return o instanceof Array;
    }-*/;
    
    public static Object as(Object obj){
    	if (!is(obj)) {
			throw new ClassCastException("Can not covert to Array");
		}
		return obj;
    }

    public static native Object[] createArray()/*-{
    return new Array();
    }-*/;

    public static native Object createObject()/*-{
    return {};
    }-*/;
}
