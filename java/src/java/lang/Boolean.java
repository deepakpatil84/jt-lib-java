/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * Original source from Google Web Toolkit(GWT) 
 * http://code.google.com/webtoolkit/
 *
 * Modified to adapt JsType Framework
 */

/* Original GWT License */

/*
 * Copyright 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package java.lang;

import com.jstype.fx.EnsureIdentifier;

import java.io.Serializable;

/**
 * <a href="http://docs.oracle.com/javase/1.5.0/docs/api/java/lang/Boolean.html">[Java Docs]</a>
 *
 */
public final class Boolean implements Serializable, Comparable<Boolean> {

    public static Boolean FALSE = new Boolean(false);
    public static Boolean TRUE = new Boolean(true);
    public static Class TYPE = Boolean.class;
    @EnsureIdentifier
    private final boolean value;

    public Boolean(boolean value) {
        this.value = value;
    }

    public Boolean(String s) {
        this(parseBoolean(s));
    }

    public boolean booleanValue() {
        return value;
    }

    public int compareTo(Boolean b) {
        return (this.value == b.value) ? 0 : (this.value ? 1 : -1);
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Boolean) && (((Boolean) obj).value == value);
    }

    /**
     * <a href="http://docs.oracle.com/javase/1.5.0/docs/api/java/lang/Boolean.html#hashCode()">[Document At]</a>
     * @return
     */
    @Override
    public int hashCode() {        
        return value ? 1231 : 1237;
    }

    public static boolean parseBoolean(String s) {
        return "true".equalsIgnoreCase(s);
    }

    @Override
    public String toString() {
        return toString(value);
    }

    public static String toString(boolean b) {
        return  b ? "true" : "false";
    }

    public static Boolean valueOf(boolean b) {
        return b ? TRUE : FALSE;
    }

    public static Boolean valueOf(String s) {
        return valueOf(parseBoolean(s));
    }
}
