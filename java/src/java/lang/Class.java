/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package java.lang;

import com.jstype.fx.EnsureIdentifier;


public final class Class<T> {

    @EnsureIdentifier
    private static Object __CLS=init();
    private String name;
    static {
	init();
    }
    private static native Object init()/*-{
    	if(!@java.lang.Class::__CLS){
	   @java.lang.Class::__CLS={};
	}
	return @java.lang.Class::__CLS;	
    }-*/;
    private Class() {
        name="";
    }
    private Class(String name){
	init();
        this.name=name;
    }

    public String getName() {
        return name;
    }
    /*TODO:Implement ClassNotFoundException
     * 
     */
    public static Class<?> forName(String className) throws Exception{
	
	Class<?> rvalue=forName0(className);
	
	if(rvalue == null){
	    throw new Exception("ClassNotFoundException");
	}	
	return rvalue;
    }
    
    private static native Class<?> forName0(String className)/*-{
	return @java.lang.Class::__CLS[className] || null;
    }-*/;
}
