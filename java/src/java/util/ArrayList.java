/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * Original source from Google Web Toolkit(GWT) 
 * http://code.google.com/webtoolkit/
 *
 * Modified to adapt JsType Framework
 */

/* Original GWT License */

/*
 * Copyright 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package java.util;

public class ArrayList<E> extends AbstractList<E> implements List<E>,RandomAccess {

    private int size = 0;
    private E element_type;

    public ArrayList() {
    }

    public ArrayList(Collection<? extends E> c) {
        spliceArray(array, 0, 0, c.toArray());
        size = array.length;
    }

    public ArrayList(int initialCapacity) {
        setCapacity(array, initialCapacity);
    }

    @Override
    public boolean add(E o) {
        array[size++] = o;
        return true;
    }
    @Override
    public void add(int index, E o) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index to add : " + index + " Current Size: " + size);
        }
        splice(array, index, 0, o);
        ++size;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        Object[] cArray = c.toArray();
        int len = cArray.length;
        if (len == 0) {
            return false;
        }
        spliceArray(array, size, 0, cArray);
        size = size+ len;
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        if (index < 0 || index > size) {
             throw new IndexOutOfBoundsException("Index to add : " + index + " Current Size: " + size);
        }
        Object[] col = c.toArray();
        int len = col.length;
        if (len == 0) {
            return false;
        }
        spliceArray(array, index, 0, col);
        size = size+ len;
        return true;
    }

    @Override
    public void clear() {
        array = (E[]) new Object[0];
        size = 0;
    }

    public Object clone() {
        return new ArrayList<E>(this);
    }
    @Override
    public boolean contains(Object elem) {
        return indexOf(elem)>=0;
    }

    public void ensureCapacity(int minCapacity) {
        if (minCapacity > size) {
            setCapacity(array, minCapacity);
        }
    }

    @Override
    public E get(int index) {
        if (index < 0 || index > size) {
             throw new IndexOutOfBoundsException("Index to add : " + index + " Current Size: " + size);
        }
        return array[index];
    }

    @Override
    public int indexOf(Object o) {
        return indexOf(o, 0);
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int lastIndexOf(Object elem) {
        return lastIndexOf(elem, size - 1);
    }

    @Override
    public E remove(int index) {
        E p = get(index);
        splice(array, index, 1);
        --size;
        return p;
    }

    @Override
    public boolean remove(Object o) {
        int i = indexOf(o);
        if (i == -1) {
            return false;
        }
        remove(i);
        return true;
    }
    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        if (fromIndex < 0 || fromIndex > size) {
             throw new IndexOutOfBoundsException("Index to start : " + fromIndex + " Current Size: " + size);
        }
        if (toIndex < 0 || toIndex > size) {
             throw new IndexOutOfBoundsException("Index to end : " + toIndex + " Current Size: " + size);
        }
        if (toIndex < fromIndex) {
            throw new IndexOutOfBoundsException("toIndex is less than fromIndex. toIndex :"+toIndex+" fromIndex :"+fromIndex);
        }
        int count = toIndex - fromIndex;
        splice(array, fromIndex, count);
        size -= count;
    }

    @Override
    public E set(int index, E element) {
        E p = get(index);
        array[index] = element;
        return p;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E[] toArray() {        
        return com.jstype.fx.Util.<E>clone(array);

    }
    @Override
    public <T> T[] toArray(T[] out) {
        if (out.length < size) {
            out = Array.createFrom(out, size);
        }
        for (int i = 0; i < size; ++i) {
            out[i] = (T) array[i];
        }
        if (out.length > size) {
            out[size] = null;
        }
        return out;
    }

    public void trimToSize() {
        setCapacity(array, size);
    }
    
    public boolean containsAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean removeAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean retainAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private static native void setCapacity(Object[] array, int newSize) /*-{
    array.length = newSize;
    }-*/;

    private static native void splice(Object[] array, int index, int deleteCount) /*-{
        array.splice(index, deleteCount);
    }-*/;

    private static native void splice(Object[] array, int index, int deleteCount,Object value) /*-{
        array.splice(index, deleteCount, value);
    }-*/;

    private static native void spliceArray(Object[] array, int index,int deleteCount, Object[] values) /*-{
        Array.prototype.splice.apply(array, [index, deleteCount].concat(values));
    }-*/;
    /**
     * This field holds a JavaScript array.
     */
    private transient E[] array = (E[]) new Object[0];
    /**
     * Ensures that RPC will consider type parameter E to be exposed. It will be
     * pruned by dead code elimination.
     */
  
    

    int capacity() {
        return array.length;
    }

    int indexOf(Object o, int index) {
        for (; index < size; ++index) {
            if (Utility.equalsWithNullCheck(o, array[index])) {
                return index;
            }
        }
        return -1;
    }

    int lastIndexOf(Object o, int index) {
        for (; index >= 0; --index) {
            if (Utility.equalsWithNullCheck(o, array[index])) {
                return index;
            }
        }
        return -1;
    }

    void setSize(int newSize) {
        setCapacity(array, newSize);
        size = newSize;
    }
}
