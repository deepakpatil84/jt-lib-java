/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * Original source from Google Web Toolkit(GWT) 
 * http://code.google.com/webtoolkit/
 *
 * Modified to adapt JsType Framework
 */

/* Original GWT License */

/*
 * Copyright 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package java.util;

import com.jstype.core.JSDate;

/**
 * Represents a date and time.
 */
public class Date implements Cloneable, Comparable<Date> {

    private static class Data {

        public static final String[] DAYS = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    }
    private final JSDate nativedate;

    public Date() {
        nativedate = JSDate.create();
    }

    public Date(int year, int month, int date) {
        this(year, month, date, 0, 0, 0);
    }

    public Date(int year, int month, int date, int hrs, int min) {
        this(year, month, date, hrs, min, 0);
    }

    public Date(int year, int month, int date, int hrs, int min, int sec) {
        nativedate = JSDate.create();
        nativedate.setFullYear(year + 1900, month, date);
        nativedate.setHours(hrs, min, sec, 0);
    }

    public Date(long date) {
        nativedate = JSDate.create(date);
    }

    public Date(String s) {
        this(Date.parse(s));
    }

    /**
     * For use by {@link #createFrom(double)}, should inline away.
     */
    Date(double milliseconds, boolean dummyArgForOverloadResolution) {
        nativedate = JSDate.create(milliseconds);
    }

    public boolean after(Date when) {
        return getTime() > when.getTime();
    }

    public boolean before(Date when) {
        return getTime() < when.getTime();
    }

    public Object clone() {
        return new Date(getTime());
    }

    public int compareTo(Date anotherDate) {
        return Long.signum(getTime() - anotherDate.getTime());
    }

    @Override
    public boolean equals(Object obj) {
        return ((obj instanceof Date) && (getTime() == ((Date) obj).getTime()));
    }

    public int getDate() {
        return nativedate.getDate();
    }

    public int getDay() {
        return nativedate.getDay();
    }

    public int getHours() {
        return nativedate.getHours();
    }

    public int getMinutes() {
        return nativedate.getMinutes();
    }

    public int getMonth() {
        return nativedate.getMonth();
    }

    public int getSeconds() {
        return nativedate.getSeconds();
    }

    public long getTime() {
        return (long) nativedate.getTime();
    }

    public int getTimezoneOffset() {
        return nativedate.getTimezoneOffset();
    }

    public int getYear() {
        return nativedate.getFullYear() - 1900;
    }

    @Override
    public int hashCode() {
        long time = getTime();
        return (int) (time ^ (time >>> 32));
    }

    public static long parse(String s) {
        double parsed = JSDate.parse(s);
        if (Double.isNaN(parsed)) {
            throw new IllegalArgumentException();
        }
        return (long) parsed;
    }

    public void setDate(int date) {
        int hours = nativedate.getHours();
        nativedate.setDate(date);
    }

    public void setHours(int hours) {
        nativedate.setHours(hours);
    }

    public void setMinutes(int minutes) {
        int hours = getHours() + minutes / 60;
        nativedate.setMinutes(minutes);
    }

    public void setMonth(int month) {
        int hours = nativedate.getHours();
        nativedate.setMonth(month);
    }

    public void setSeconds(int seconds) {
        int hours = getHours() + seconds / (60 * 60);
        nativedate.setSeconds(seconds);
    }

    public void setTime(long time) {
        nativedate.setTime(time);
    }

    public void setYear(int year) {
        int hours = nativedate.getHours();
        nativedate.setFullYear(year + 1900);
    }

    public String toGMTString() {
        return nativedate.getUTCDate() + " " + Data.MONTHS[nativedate.getUTCMonth()]
                + " " + nativedate.getUTCFullYear() + " " + padTwo(nativedate.getUTCHours())
                + ":" + padTwo(nativedate.getUTCMinutes()) + ":"
                + padTwo(nativedate.getUTCSeconds()) + " GMT";
    }

    public String toLocaleString() {
        return nativedate.toLocaleString();
    }

    @Override
    public String toString() {
        // Compute timezone offset. The value that getTimezoneOffset returns is
        // backwards for the transformation that we want.
        int offset = -nativedate.getTimezoneOffset();
        String hourOffset = ((offset >= 0) ? "+" : "") + (offset / 60);
        String minuteOffset = padTwo(Math.abs(offset) % 60);

        return Data.DAYS[nativedate.getDay()] + " "
                + Data.MONTHS[nativedate.getMonth()] + " " + padTwo(nativedate.getDate())
                + " " + padTwo(nativedate.getHours()) + ":" + padTwo(nativedate.getMinutes())
                + ":" + padTwo(nativedate.getSeconds()) + " GMT" + hourOffset
                + minuteOffset + " " + nativedate.getFullYear();
    }

    public static long UTC(int year, int month, int date, int hrs, int min, int sec) {
        return (long) JSDate.UTC(year + 1900, month, date, hrs, min, sec, 0);
    }

    protected static String padTwo(int number) {
        if (number < 10) {
            return "0" + number;
        } else {
            return String.valueOf(number);
        }
    }

    /**
     * Package private factory for JSNI use, to allow cheap creation of dates from
     * doubles.
     */
    static Date createFrom(double milliseconds) {
        return new Date(milliseconds, false);
    }
}
