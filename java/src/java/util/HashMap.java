/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * Original source from Google Web Toolkit(GWT) 
 * http://code.google.com/webtoolkit/
 *
 * Modified to adapt JsType Framework
 */

/* Original GWT License */

/*
 * Copyright 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package java.util;

public class HashMap<K, V> extends AbstractHashMap<K, V> implements Cloneable {

    private K key_type;
    private V value_type;

    public HashMap() {
    }

    public HashMap(int ignored) {
        super(ignored);
    }

    public HashMap(int ignored, float alsoIgnored) {
        super(ignored, alsoIgnored);
    }

    public HashMap(Map<? extends K, ? extends V> toBeCopied) {
        super(toBeCopied);
    }

    @Override
    public Object clone() {
        return new HashMap<K, V>(this);
    }

    @Override
    protected boolean equals(Object value1, Object value2) {
        return Utility.equalsWithNullCheck(value1, value2);
    }

    @Override
    protected int getHashCode(Object key) {
        return ~ ~key.hashCode();
    }
}
