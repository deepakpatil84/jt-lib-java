/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * Original source from Google Web Toolkit(GWT) 
 * http://code.google.com/webtoolkit/
 *
 * Modified to adapt JsType Framework
 */

/* Original GWT License */

/*
 * Copyright 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package java.util;

public abstract class AbstractSet<E> extends AbstractCollection<E> implements Set<E> {

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Set)) {
            return false;
        }

        Set<?> other = (Set<?>) o;

        if (other.size() != size()) {
            return false;
        }

        for (Iterator<?> iter = other.iterator(); iter.hasNext();) {
            Object otherItem = iter.next();
            if (!contains(otherItem)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        for (Iterator<E> iter = iterator(); iter.hasNext();) {
            E next = iter.next();
            if (next != null) {
                hashCode += next.hashCode();
                hashCode = ~~hashCode;
            }
        }
        return hashCode;
    }

    @Override
    public boolean removeAll(Collection<? extends E> c) {
        int size = size();
        if (size < c.size()) {
            for (Iterator<E> iter = iterator(); iter.hasNext();) {
                E o = iter.next();
                if (c.contains(o)) {
                    iter.remove();
                }
            }
        } else {
            for (Iterator<?> iter = c.iterator(); iter.hasNext();) {
                Object o = iter.next();
                remove(o);
            }
        }
        return (size != size());
    }
}
